package com.example.assignment1;

import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends Activity {

	public final static String EXTRA_MESSAGE = "com.example.assignment1.MESSAGE";
	public final static String PREFS_NAME = "ass1_saved_data";
	private Boolean skipNextSelectedEvent = true;
	private Boolean isFirstResume = true;
	private SharedPreferences preferences;
	private Boolean isShowingImage = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		updateSpinner();
		
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		// Restore URL textbox
		final EditText editText = (EditText) findViewById(R.id.textbox_url);
		editText.setText(preferences.getString("current_url", ""));
		
		// Restore History spinner
		final Spinner spinner = (Spinner)findViewById(R.id.spinner_prevurls);	
		try {
			spinner.setSelection(preferences.getInt("spinner_selected_index", 0));
		} catch (Exception e) {} // Just have a catch here in-case the index is out of range.
		
		// Set spinner to affect url textbox
		spinner.setOnItemSelectedListener (new OnItemSelectedListener() {
			
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				
				if(skipNextSelectedEvent) {
					skipNextSelectedEvent = false;
					return;
				}
				
				editText.setText(spinner.getSelectedItem().toString());
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				onItemSelected(arg0, null, 0, 0);
			}
	    });
		
		// Download image if that was previous state
	    if(preferences.getBoolean("is_showing_image", false)) {
	    	onButtonDownloadClick(null);
		}
	}
	
	private void updateSpinner() {
		// Fetch the spinner
		Spinner spinner = (Spinner)findViewById(R.id.spinner_prevurls);	
		int pos = spinner.getSelectedItemPosition();
		
		ArrayList<String> entries = new ArrayList<String>();
		Ass1DatabaseHelper dbHelper = new Ass1DatabaseHelper(this);
		
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery(Ass1DatabaseHelper.TABLE_GET_CONTENT_QUERY, null);
		
		if (cursor.moveToFirst()) {
            do {
            	if(cursor.getString(0).length() > 1)
            		entries.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, entries);
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    Spinner Items = (Spinner) findViewById(R.id.spinner_prevurls);
	    skipNextSelectedEvent = true;
	    Items.setAdapter(adapter);
	   
	    // Change the index while making sure it won't affect the URL text-box.
	    skipNextSelectedEvent = true;
	    spinner.setSelection(pos, false);
	    skipNextSelectedEvent = false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onButtonDownloadClick(View view) {
		
		// Get the URL and check if it's empty.
		EditText editText = (EditText) findViewById(R.id.textbox_url);
		String message = editText.getText().toString();
		if(message.length() < 10)
			return;
		
		// Set up the activity for downloading iamge
		Intent intent = new Intent(this, DownloadImageActivity.class);
		intent.putExtra(EXTRA_MESSAGE, message);
		
		// Save URL to database
		Ass1DatabaseHelper dbHelper = new Ass1DatabaseHelper(this);
		final ContentValues values = new ContentValues();
		values.put("url", message);
		
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
		db.insert(Ass1DatabaseHelper.TABLE_NAME, null, values);
		
		// Start the other activity
		startActivity(intent);
		updateSpinner();
		isShowingImage = true;
	}
	
	public void onButtonExampleURL(View view) {
		EditText textEdit = (EditText)findViewById(R.id.textbox_url);
		textEdit.setText("http://dl.dropboxusercontent.com/u/1564029/Example.png");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		// Save preferences.
		SharedPreferences.Editor editor = preferences.edit();
		EditText urlTextbox = (EditText)findViewById(R.id.textbox_url);
		Spinner spinner = (Spinner)findViewById(R.id.spinner_prevurls);	
		
		editor.putBoolean("is_showing_image", isShowingImage);
		editor.putString("current_url", urlTextbox.getText().toString());
		editor.putInt("spinner_selected_index", spinner.getSelectedItemPosition());
		editor.commit();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if(isShowingImage)
			isShowingImage = false;
		
		if(!isFirstResume) {
			// Save preferences.
			SharedPreferences.Editor editor = preferences.edit();
			editor.putBoolean("is_showing_image", isShowingImage);
			editor.commit();
		}
		
		isFirstResume = false;
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}
}
