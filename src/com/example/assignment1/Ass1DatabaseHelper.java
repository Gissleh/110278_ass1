package com.example.assignment1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class Ass1DatabaseHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "notes.db";
	public static final int DATABASE_VERSION = 1;
		
	public static final String TABLE_NAME = "previousurls";
	
	public static final String TABLE_CREATION_QUERY = "CREATE TABLE previousurls (id INTEGER PRIMARY KEY, url VARCHAR(255) UNIQUE);";
	public static final String TABLE_GET_CONTENT_QUERY = "SELECT url FROM `previousurls` ORDER BY id DESC LIMIT 256";
	
	public Ass1DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_CREATION_QUERY);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
